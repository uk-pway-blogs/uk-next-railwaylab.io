---
title: Pinch Points

tags:
  - Pinch Points
  - Gareth Dennis
---

The UK's CH4 show dispatches had an episode called [Britain's Train Hell](https://www.channel4.com/programmes/dispatches/on-demand/71118-001)
with the professional Gareth Dennis given a few moments

Gareth then did a Live YouTube.Q&A and this is a followup page to help along newbies... Diolch yn fawr Gareth.boyo!


<iframe width="560" height="315" src="https://www.youtube.com/embed/Pe7g5GALvc4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


- 34.00: [Didcot](/locations/didcot)

- 34.00: [Ely North](/locations/ely-north)

- 34.00: [Norwich south](/locations/norwich-south)


