

var topLeft = L.latLng(53.6, -5.4);
var bottomRight = L.latLng(51.4, -2.75);

var stationMarkerOptions = {
    radius: 8,
    fillColor: "#ff7800",
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
};

var WelshBounds = L.latLngBounds(topLeft, bottomRight);


function CreateMap(){

    //var pos = [53.40918771491449, -4.345552325533347]

    var map = L.map('mapid', {
        center: WelshBounds.getCenter(),
        maxBounds: WelshBounds,
        maxBoundsViscosity: 1.0

    }).setView(WelshBounds.getCenter(), 13);
    //map.setMaxBounds(WelshBounds);

    //alert({{page.meta.lat}})

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoicGVkcm9tb3JnYW4iLCJhIjoiY2s3Nzg4azF3MDRwdzNvcGh5cXE5bjAyZiJ9.Bry79aB3OoladY6R94rxYQ'
    }).addTo(map);

    return map;

}

function SetupLineMap(){


    var map = CreateMap()

    var PATH = window.location.pathname

    for(var i =0; i < DATA.stations.length; i++){
        var sta = DATA.stations[i];
        //#console.log(sta);
        var marker = L.marker([sta.position.lng, sta.position.lat], stationMarkerOptions);
        marker.bindTooltip(sta.title, {permanent: true, opacity: 0.7}).openTooltip();

        s = "<ul>"
        s += '<li>Station: <a href="' + sta.path + '">' + sta.title + "</a></li>"
        s += '<li>Line: <a href="' + DATA.path + '">' + DATA.title + "</a></li>"
        marker.bindPopup(s);
        marker.addTo(map);
    }

    var geojsonLayer = new L.GeoJSON.AJAX(PATH + "/data.geojson", {
        pointToLayer: function (feature, latlng) {
            return
            console.log(feature.properties)
            var label = String(feature.properties.name);
            var marker = L.marker(latlng, stationMarkerOptions);

            //marker.bindPopup('<p></p>');
            marker.bindTooltip(label, {permanent: true, opacity: 0.6}).openTooltip();
//            var marker =  L.marker(latlng, stationMarkerOptions

//
//            marker.bindTooltip(lsabel, {permanent: true, opacity: 0.7}).opesnTooltip();
//
//            marker.on('click', function (e) {
//              this.openssPopup();
//              //disable mouseout behavior here?
//            });
//            return marker;

            return marker;
        },
        style: function(feature) {
            //console.log("===", feature.properties)
            return {color: "#333333", weight: 2, dashArray: "10 10",}
        }
    });
    geojsonLayer.on('data:loaded', function() {
       map.fitBounds(geojsonLayer.getBounds())
    });
    geojsonLayer.addTo(map);


} // func linemap <<

function SetupMegaMap(){


    var map = CreateMap()

    var geojsonLayer = new L.GeoJSON.AJAX("/index.geojson", {
        pointToLayer: function (feature, latlng) {
            var label = String(feature.properties.name);

            var marker =  L.marker(latlng, stationMarkerOptions
            ).bindPopup('<p>You are here sss</p>')

            marker.bindTooltip(lsabel, {permanent: true, opacity: 0.7}).opesnTooltip();

            marker.on('click', function (e) {
              this.openssPopup();
              //disable mouseout behavior here?
            });
            return marker;
        },
        style: function(feature) {
            //console.log("===", feature.properties)
            return {color: "#999999"}
        }
    });
    geojsonLayer.on('data:loaded', function() {
       map.fitBounds(geojsonLayer.getBounds())
    });
    geojsonLayer.addTo(map);




} // func linemap <<




